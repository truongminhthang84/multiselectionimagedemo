

import UIKit

class CollectionViewCell: UICollectionViewCell {
    var itemViewModel: ItemViewModel? {
        didSet {
            
            guard let itemViewModel = itemViewModel else {return}
            representedAssetIdentifier = itemViewModel.representedAssetIdentifier
            itemViewModel.thumbnailImage.addObserver { [weak self](image) in
                guard let self = self else {return}
                if itemViewModel.representedAssetIdentifier == self.representedAssetIdentifier {
                    self.imageView.image = image
                }
            }
            
            itemViewModel.isSelected.addObserver { [weak self](isSelected) in
                guard let self = self else {return}
                if itemViewModel.representedAssetIdentifier == self.representedAssetIdentifier {
                    self.selectedIndexLabel.isHidden = !isSelected
                    self.borderColor = isSelected ? UIColor.blue : UIColor.white
                }
            }

            
            
        }
    }
    
    var representedAssetIdentifier: String!
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var selectedIndexLabel: UILabel!
    
    

    
    
}

