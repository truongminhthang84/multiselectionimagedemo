////
////  PhotoCollectionViewDelegate.swift
////  ImagePickerDemo
////
////  Created by ThangTM-PC on 1/10/20.
////  Copyright © 2020 ThangTM-PC. All rights reserved.
////
//
import UIKit
import DataBinding

class PhotoCollectionViewDelegate: NSObject, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    private let numberOfItemsPerRow: CGFloat
    private let interItemSpacing: CGFloat
    private let itemSize: CGSize
    static var thumbnailSize: CGSize = .zero
    
    private weak var itemsLiveData : MutableLiveData<[ItemViewModel]>?

    init(itemSize: CGSize, numberOfItemsPerRow: CGFloat, interItemSpacing: CGFloat) {
        self.itemSize = itemSize
        self.numberOfItemsPerRow = numberOfItemsPerRow
        self.interItemSpacing = interItemSpacing
    }

    func bind(items liveData: MutableLiveData<[ItemViewModel]>) {
        itemsLiveData = liveData
    }
    
    deinit {
        print("Deinit:\t" + String(describing: Self.self))
    }
   
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
     
      return itemSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
      return interItemSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
      return interItemSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: interItemSpacing/2, left: 0, bottom: interItemSpacing/2, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        itemsLiveData?.value[indexPath.row].isSelected.toggle()
        collectionView.reloadItems(at: [indexPath])
    }
    
    
//    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
//       // prefetchItem
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cancelPrefetchingForItemsAt indexPaths: [IndexPath]) {
//
//    }
    
//    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//
//    }
//
//    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//        itemsLiveData?.value[indexPath.row].stopCachingImages()
//
//    }
    
    
}

//
//
//
//
//
