//////
//////  DataSource.swift
//////  ImagePickerDemo
//////
//////  Created by ThangTM-PC on 1/9/20.
//////  Copyright © 2020 ThangTM-PC. All rights reserved.
//////
////
////import UIKit
////import DataBinding
////
//open class CollectionViewDataSource<CellType: UICollectionViewCell, ItemViewModel: Selectable & Equatable>: NSObject, UICollectionViewDataSource {
//    
//    let cellId: String
//    var itemsLiveData : MutableLiveData<[ItemViewModel]>?
//    let noContentView: UIView
//    var items: [ItemViewModel] = [] {
//        didSet {
//            DispatchQueue.main.async {
//                self.collectionView?.reloadData()
//            }
//        }
//    }
//    let configure: (CellType, ItemViewModel) -> ()
//    
//    weak var collectionView: UICollectionView?
//    
//    init(viewModel: ViewModel, cellId: String, noContentView: UIView, configure: @escaping (CellType, ItemViewModel) -> Void) {
//        self.cellId = cellId
//        self.configure = configure
//        self.noContentView = noContentView
//        super.init()
//    }
//   
//    func insertItem(_ item: ItemViewModel, at index: Int) {
//        self.items.insert(item, at: index)
//        if items.count == 1 {
//            collectionView?.reloadData()
//        } else {
//            collectionView?.insertItems(at: [IndexPath(row: items.count - 1, section: 0)])
//        }
//    }
//    
//    open func removeItems(_ item: ItemViewModel, at index: Int) {
//        self.items.remove(at: index)
//    }
//    
//    deinit {
//        print("Deinit:\t" + String(describing: Self.self))
//    }
//    
//    //MARK: UICollectionViewDataSource
//
//    
//    open func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//          if self.collectionView == nil {
//                  self.collectionView = collectionView
//              }
//        return items.count
//    }
//    
//    open func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//         let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! CellType
//        let vm = items[indexPath.row]
//        configure(cell, vm)
//        
//        return cell
//    }
//}
//
////
////
////
////
