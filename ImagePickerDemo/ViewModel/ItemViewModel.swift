//
//  ItemViewModel.swift
//  ImagePickerDemo
//
//  Created by ThangTM-PC on 1/9/20.
//  Copyright © 2020 ThangTM-PC. All rights reserved.
//

import UIKit
import Photos
import DataBinding

class ItemViewModel: Selectable {
   
    
    var representedAssetIdentifier: String
    private let id = UUID().uuidString
    
    var thumbnailImage = MutableLiveData<UIImage?>(nil)
    var selectedNumber = MutableLiveData<Int?>(nil)
    var isSelected = MutableLiveData<Bool>(false)
    var asset: PHAsset
    init(asset: PHAsset) {
        self.asset = asset
        self.representedAssetIdentifier = asset.localIdentifier
    }
    
    func fetchImage(imageManager: PHCachingImageManager, thumbnailSize: CGSize) {
        imageManager.requestImage(for: asset, targetSize: thumbnailSize, contentMode: .aspectFill, options: nil, resultHandler: {[weak self] image, _ in
            // The cell may have been recycled by the time this handler gets called;
            // set the cell's thumbnail image only if it's still showing the same asset.
//            if cell.representedAssetIdentifier == asset.localIdentifier {
//
//            }
            self?.thumbnailImage.value = image
            
        })
    }
    
}


public protocol Selectable {
    var isSelected: MutableLiveData<Bool> {get set}
    
}

// MARK: - <#Mark#>

extension Selectable {
    public mutating func setSelected(_ selected: Bool) {
        isSelected.value = selected
    }
    
    public mutating func toggle() {
        isSelected.value = !isSelected.value
    }
}

