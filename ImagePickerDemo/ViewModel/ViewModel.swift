////
////  ViewModel.swift
////  ImagePickerDemo
////
////  Created by ThangTM-PC on 1/9/20.
////  Copyright © 2020 ThangTM-PC. All rights reserved.
////

import UIKit
import DataBinding
import Photos

class ViewModel {
    var imageManager = PHCachingImageManager()

    var itemViewModels = MutableLiveData<[ItemViewModel]>([])
    
    var selectedImage = MutableLiveData<[ItemViewModel]>([])
    
    init() {
       
    }

    func addAssets(addedAssets: [PHAsset],removedAssets: [PHAsset] , thumbnailSize: CGSize) {

        imageManager.startCachingImages(for: addedAssets,
                                        targetSize: thumbnailSize, contentMode: .aspectFill, options: nil)
        imageManager.stopCachingImages(for: removedAssets,
                                       targetSize: thumbnailSize, contentMode: .aspectFill, options: nil)
        itemViewModels.value += addedAssets.map { (asset) in
            ItemViewModel(asset: asset)
        }
    }
    func resetCachedAssets() {
        imageManager.stopCachingImagesForAllAssets()
        itemViewModels.value = []
    }

    func fetchImage(indexPath: IndexPath, thumbnailSize: CGSize) {
        itemViewModels.value[indexPath.item].fetchImage(imageManager: imageManager, thumbnailSize: thumbnailSize)
    }
    
    func selected(at indexPath: IndexPath) {
        itemViewModels.value[indexPath.item].isSelected.toggle()
    }
    

}
