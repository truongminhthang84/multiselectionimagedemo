////
////  ViewController.swift
////  ImagePickerDemo
////
////  Created by ThangTM-PC on 1/8/20.
////  Copyright © 2020 ThangTM-PC. All rights reserved.
////
//
//import UIKit
//import DataBinding
//
//
//@available(iOS 13.0, *)
//class NewPhotosViewController: UICollectionViewController {
//    
//    @IBOutlet weak var collectionViewFlowLayout: UICollectionViewFlowLayout!
//    
//    var viewModel = ViewModel()
//    
//    var dataSource: UICollectionViewDiffableDataSource<Section, ItemViewModel>!
//    
//    var initialSnapshot = NSDiffableDataSourceSnapshot<Section, ItemViewModel>()
//    
//    enum Section {
//        case main
//    }
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        collectionView.collectionViewLayout = configureLayout()
//        configureDataSource()
//        viewModel.itemViewModels.addObserver { [weak self] (itemViewModels) in
//            guard let self = self else {return}
//            self.initialSnapshot.appendItems(itemViewModels)
//            self.dataSource.apply(self.initialSnapshot, animatingDifferences: false)
//        }
//    }
//    
//    deinit {
//        print("deinit")
//    }
//    
//    func configureLayout() -> UICollectionViewCompositionalLayout {
//        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.25), heightDimension: .fractionalHeight(1.0))
//        let item = NSCollectionLayoutItem(layoutSize: itemSize)
//        item.contentInsets = NSDirectionalEdgeInsets(top: 1, leading: 1, bottom: 1, trailing: 1)
//        
//        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalWidth(0.25))
//        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitems: [item])
//        
//        let section = NSCollectionLayoutSection(group: group)
//        
//        return UICollectionViewCompositionalLayout(section: section)
//    }
//    
//    func configureDataSource() {
//        dataSource = UICollectionViewDiffableDataSource<Section, ItemViewModel>(collectionView: collectionView) { (collectionView, indexPath, itemViewModel) -> UICollectionViewCell? in
//            
//            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionViewCell.reuseIdentifier, for: indexPath) as? CollectionViewCell else {
//                fatalError("Cannot create new cell")
//            }
//            
//            cell.itemViewModel = itemViewModel
//            
//            return cell
//        }
//        
//        initialSnapshot.appendSections([.main])
//        
//        dataSource.apply(initialSnapshot, animatingDifferences: false)
//    }
//}
