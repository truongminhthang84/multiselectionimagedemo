//
//  PhotoRepository.swift
//  ImagePickerDemo
//
//  Created by ThangTM-PC on 1/9/20.
//  Copyright © 2020 ThangTM-PC. All rights reserved.
//

import Foundation
import Photos
import DataBinding

class PhotoRepository: NSObject, PHPhotoLibraryChangeObserver {
    
    private var fetchResult : PHFetchResult<PHAsset>?
    var displayedAssets  = MutableLiveData<[PHAsset]>([])
    
    static let shared = PhotoRepository()
    fileprivate var imageManager = PHCachingImageManager()
    var nextPage : Int?
    let pageSize = 250
    var total: Int?
    var items: [PHAsset] = []
    
    override init() {
        super.init()
        reqestAuthorization()
        PHPhotoLibrary.shared().register(self)
    }
    
    deinit {
        print("Deinit:\t" + String(describing: Self.self))
        PHPhotoLibrary.shared().unregisterChangeObserver(self)
    }
    
    func unregisterChangeObserver() {
        PHPhotoLibrary.shared().unregisterChangeObserver(self)
    }
    
    
    func reqestAuthorization() {
        if PHPhotoLibrary.authorizationStatus() != .authorized {
            PHPhotoLibrary.requestAuthorization { [weak self] (status) in
                guard let self = self else {return}
                switch status {
                case .authorized:
                    self.fetchAsset()
                case .denied:
                    fallthrough
                case .notDetermined:
                    fallthrough
                case .restricted: break
                // showAlertToOpenSetting(title: "No Photo Permissions", message: "Please grant photo permissions in Settings")
                @unknown default:
                    fatalError()
                }
            }
        } else {
            self.fetchAsset()
            
        }
    }
    
    // MARK: Request fetch and update asset
    
    func fetchAsset() {
        let options = PHFetchOptions()
        options.sortDescriptors = [
            NSSortDescriptor(key: "creationDate", ascending: false)
        ]
        self.fetchResult = PHAsset.fetchAssets(with: .image, options: options)
        loadmore()
    }
    
    func loadmore() {
        let isFirstTime = (nextPage == nil && total == nil)
        let hasItemLeft = items.count < (total ?? 0)
        if  isFirstTime || hasItemLeft {
            let currentPage = nextPage ?? 0
            nextPage = currentPage + 1
            total = fetchResult?.count ?? 0
            if let nextPageStop = [(nextPage! * pageSize), (total! - 1)].min() {
                if nextPageStop < 0 {return}
                self.displayedAssets.value += fetchResult?.objects(at: IndexSet((currentPage * pageSize) ..< nextPageStop)) ?? []
            }
            
            
        }
    }
    
    // MARK: - Handle remove and insert in photo libraray
    
    
    
    func photoLibraryDidChange(_ changeInstance: PHChange) {
        
        guard let changes = changeInstance.changeDetails(for: fetchResult!)
            else { return }
        
        // Change notifications may be made on a background queue. Re-dispatch to the
        // main queue before acting on the change as we'll be updating the UI.
        DispatchQueue.main.sync {
            // Hang on to the new fetch result.
            if changes.hasIncrementalChanges {
                 self.fetchResult = changes.fetchResultAfterChanges
                
            } else {
                
            }
        }
    }
}




