//
//  PhotosViewController.swift
//  ImagePickerDemo
//
//  Created by ThangTM-PC on 1/10/20.
//  Copyright © 2020 ThangTM-PC. All rights reserved.
//
/*
import UIKit



@available(iOS 9.0, *)

class PhotosViewController: UICollectionViewController {
    var viewModel = ViewModel()

    private let numberOfItemsPerRow: CGFloat = 4
    private let interItemSpacing: CGFloat = 1

    var dataSource :  CollectionViewDataSource<CollectionViewCell, ItemViewModel>!
    
    var delegate : PhotoCollectionViewDelegate!
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataSource = CollectionViewDataSource<CollectionViewCell, ItemViewModel>(viewModel: viewModel, cellId: CollectionViewCell.reuseIdentifier,
                                                                                      noContentView: UIView(),
                                                                                      configure:
            { cell, itemViewModel in
                itemViewModel.fetchImage()
            cell.itemViewModel = itemViewModel
        })
        delegate = getDelegate()
        collectionView.dataSource = dataSource
        collectionView.delegate = delegate
       
     
    }
    
    private func getDelegate() -> PhotoCollectionViewDelegate {
        let scale = UIScreen.main.scale
        let itemSize = calculateItemSize(numberOfItemsPerRow: numberOfItemsPerRow, interItemSpacing: interItemSpacing)
        PhotoCollectionViewDelegate.thumbnailSize = CGSize(width: itemSize.width * scale, height: itemSize.height * scale)
        return PhotoCollectionViewDelegate(itemSize: itemSize, numberOfItemsPerRow: numberOfItemsPerRow, interItemSpacing: interItemSpacing)
    }
    
    private func calculateItemSize(numberOfItemsPerRow: CGFloat, interItemSpacing: CGFloat) -> CGSize {
        let maxWidth = UIScreen.main.bounds.width
        let totalSpacing = interItemSpacing * numberOfItemsPerRow
        let itemWidth = (maxWidth - totalSpacing)/numberOfItemsPerRow
        return CGSize(width: itemWidth, height: itemWidth)
    }
 
    deinit {
        print("Deinit:\t" + String(describing: Self.self))
    }
    
}
 
 */
